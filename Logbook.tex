\documentclass{article}
\usepackage[a4paper, portrait, margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{url}
\usepackage{multirow}
\usepackage{float}
\usepackage{nameref}
\title{UFMFMA-15-2 Signal Processing and Circuits}
\author{Daniel Myers - 18045657}
\graphicspath{ {images/} }

\begin{document}
  \begin{titlepage}
    \maketitle
    \pagebreak
    \tableofcontents
  \end{titlepage}

  \section{Op Amps: Input Characteristics}
    \subsection{Introduction}
      This lab involves investigating the input characteristics of an op amp, specifically the input bias current, input offset current, and input offset voltage.
      I had to do some research to learn how to measure the difference characteristics of an op amp.
      My main point of research was the MT-038 document from Analog Devices \cite{MT-038},
      I used figure 4 from this document as a reference for my simulation circuit design which I implemented using LTspice.
      This figure is shown below as figure \ref{fig:measuring input bias current} below.
      \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{opamp/measuring input bias current.png}
        \caption{Measuring Input Bias Current \cite{MT-038}}
        \label{fig:measuring input bias current}
      \end{figure}
    \subsection{LM324 – Test positive input bias current}
      My circuit design to test the positive input bias current is shown below in figure \ref{fig:IB pos test}. This is created and simulated in LTspice.
      \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{opamp/IB pos test.png}
        \caption{Circuit design to test $I_{B+}$}
        \label{fig:IB pos test}
      \end{figure}

      Figure \ref{fig:IB pos test wave} shows the output waveform from the simulation, it is a DC signal of about -197.75mV.
      \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{opamp/IB pos test wave.png}
        \caption{Output waveform testing $I_{B+}$}
        \label{fig:IB pos test wave}
      \end{figure}

      \includegraphics{opamp/IB pos test val.png}

      I repeated this method of measuring the output signal for each of the combinations of both switches being closed or open.

    \subsection{LM324 – Test negative input bias current}
      \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{opamp/IB neg test.png}
        \caption{Circuit design to test $I_{B-}$}
        \label{fig:IB neg test}
      \end{figure}

      \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{opamp/IB neg test wave.png}
        \caption{Output waveform testing $I_{B-}$}
        \label{fig:IB neg test wave}
      \end{figure}

      \includegraphics{opamp/IB neg test val.png}
      
    \subsection{LM324 – Test input offset current}
      \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{opamp/IOS test.png}
        \caption{Circuit design to test $I_{OS}$}
        \label{fig:IOS test}
      \end{figure}

      \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{opamp/IOS test wave.png}
        \caption{Output waveform testing $I_{OS}$}
        \label{fig:IOS test wave}
      \end{figure}

      \includegraphics{opamp/IOS test val.png}
    
    \subsection{LM324 – Test input offset voltage}
      \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{opamp/VOS test.png}
        \caption{Circuit design to test $V_{OS}$}
        \label{fig:VOS test}
      \end{figure}

      \begin{figure}[H]
        \centering
        \includegraphics[width=0.8\textwidth]{opamp/VOS test wave.png}
        \caption{Output waveform testing $V_{OS}$}
        \label{fig:VOS test wave}
      \end{figure}

      \includegraphics{opamp/IOS test val.png}

    \subsection{Repeating for other op amps}
      I then repeated this process of testing all the voltage outputs for 3 other different op amps, OP113, AD8565, and OP495.
      These results are collated table \ref{table:op amp output voltages} below.
      \begin{table}[H]
        \centering
        \begin{tabular}{|c|c|c|c|c|}
          \hline                & LM324        & OP113         & AD8565         & OP495       \\ 
          \hline $V_O (I_{B+})$ & -197.75461mV & -2.67243mV    & -755.31018mV   & 106.2852mV  \\  
          \hline $V_O (I_{B-})$ & 204.73792mV  & 2.17161mV     & 655.28518mV    & 267.57422mV \\
          \hline $V_O (I_{OS})$ & 3.584058mV   & -502.895240mV & 726.93615µV    & 194.79007mV \\
          \hline $V_O (V_{OS})$ & 3.4015714mV  & 2.0749304mV   & -100.2596100mV & 179.06936mV \\
          \hline    
        \end{tabular}
        \caption{Op amp output voltages}
        \label{table:op amp output voltages}
      \end{table}

    \subsection{Calculating actual characteristics}
      These values then need to be converted to their respective characteristics that they represent.
      \subsubsection{Positive input bias current}
        \[V_O=+[1+\frac{R6}{100}] I_{B+} R_S\]
        This equation is shown in Figure \ref{fig:measuring input bias current} - \nameref{fig:measuring input bias current}
        with a slight modification changing R2 to R6 so it matches the correct resistor from the circuit design in LTspice.

        This can then be rearranged making $I_{B+}$ the subject, as shown below.
        \[I_{B+}=\frac{V_O}{R_S[1+\frac{R6}{100}]}\]
        The values of the resistors can be substituted into the equation,
        R6 is equal to 10,000 and RS is equal to the value of the resistor in series with the input under test, in both cases it will be 100,000.
        Finally, the voltage will be substituted in and the result for $I_{B+}$ can be calculated.
      
      \subsubsection{Negative input bias current}
        \[V_O=-[1+\frac{R6}{100}] I_{B-} R_S\]
        This equation is shown in Figure \ref{fig:measuring input bias current} - \nameref{fig:measuring input bias current}
        with the same modification changing R2 to R6 so it matches the correct resistor from the circuit design in LTspice.

        This can then be rearranged making $I_{B-}$ the subject, as shown below.
        \[I_{B-}=\frac{V_O}{-R_S[1+\frac{R6}{100}]}\]
        The values of the resistors can be substituted into the equation,
        R6 is equal to 10,000 and RS is equal to the value of the resistor in series with the input under test, in both cases it will be 100,000.
        Finally, the voltage will be substituted in and the result for $I_{B-}$ can be calculated.
      
      \subsubsection{Input offset current}
        \[V_O=[1+\frac{R6}{100}] I_{OS}\]
        This equation is shown in Figure \ref{fig:measuring input bias current} - \nameref{fig:measuring input bias current}
        with the same modification changing R2 to R6 so it matches the correct resistor from the circuit design in LTspice.

        This can then be rearranged making $I_{OS}$ the subject, as shown below.
        \[I_{OS}=\frac{V_O}{[1+\frac{R6}{100}]}\]
        The value of the resistor can be substituted into the equation, R6 is equal to 10,000.
        Finally, the voltage will be substituted in and the result for $I_{OS}$ can be calculated.

      \subsubsection{Input offset voltage}
        \[V_O=[1+\frac{R6}{100}] V_{OS}\]
        This equation is shown in Figure \ref{fig:measuring input bias current} - \nameref{fig:measuring input bias current}
        with the same modification changing R2 to R6 so it matches the correct resistor from the circuit design in LTspice.

        This can then be rearranged making $V_{OS}$ the subject, as shown below.
        \[V_{OS}=\frac{V_O}{[1+\frac{R6}{100}]}\]
        The value of the resistor can be substituted into the equation, R6 is equal to 10,000.
        Finally, the voltage will be substituted in and the result for $V_{OS}$ can be calculated.

    \subsection{Conclusion}
      \subsubsection{Summary of results}
        \begin{table}[H]
          \centering
          \begin{tabular}{|c|c|c|c|c|}
            \hline          & LM324    & OP113     & AD8565    & OP495    \\ 
            \hline $I_{B+}$ & -19.58nA & -264.60pA & -74.78nA  & 10.52nA  \\  
            \hline $I_{B-}$ & -20.27nA & -215.01pA & -64.88nA  & -26.49nA \\
            \hline $I_{OS}$ & 35.49µA  & -4.98mA   & 7.20µV    & 1.93mA   \\
            \hline $V_{OS}$ & 33.68µV  & 20.54µV   & -992.67µV & 1.77mA   \\
            \hline $I_{B}$  & -19.93nA & -239.80pA & -69.83nA  & -7.98nA  \\
            \hline    
          \end{tabular}
          \caption{Op amp input characteristics results}
          \label{table:op amp input characteristics results}
        \end{table}

        The table of results shows how much the characteristics can vary across different operation amplifiers.
        This means that some op amps may be better suited to different tasks compared to others.

      \subsubsection{Reflection}
        This task was quite difficult for me as I've had very limited previous experiences with op amps.
        However, it has helped me gain more of an understanding about them and should benefit me in the future.

      \subsubsection{Research Piece}
        The input bias characteristics of an operational amplifier can be very important and could possibly have a large negative effect on the system.
        If the bias currents of the op amp are very similar, a compensating resistor can be used to create a voltage drop at the non-inverting input.
        Figure \ref{fig:canceling the effects of input bias current} below was taken from \nameref{fig:measuring input bias current} and helps to explain this further.

        \begin{figure}[H]
          \centering
          \includegraphics[width=0.5\textwidth]{opamp/canceling the effects of input bias current.png}
          \caption{Canceling the Effects of Input Bias Current within an Application }
          \label{fig:canceling the effects of input bias current}
        \end{figure}

        Precision op amps can be purchased which will have lower input bias however these can be quite expensive.
        An op amp with FET inputs would also reduce the input bias current. \cite{Op-AmpBias}

        Any offset voltage would be multiplied by the gain of the op amp in a closed loop implementation, therefore causing an increasing degree of inaccuracy.
      
  \newpage
  \section{Op Amps: AC Considerations}
    \subsection{Introduction}
        For this lab, I will investigate some of the other characteristics of an operational amplifier, this includes the gain/bandwidth and the slew rate.
        It didn't require as much research compared to the previous lab.
    \subsection{Gain / Bandwidth Stability}
      \subsubsection{Open Loop Op Amp}
        I first set up an op amp in open loop configuration, this is shown in figure \ref{fig:inverting open loop}.
        A 20mV peak to peak sine wave signal at 1KHz is used as the input.
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.8\textwidth]{opamp/inverting open loop.png}
          \caption{Open Loop Op Amp Circuit}
          \label{fig:inverting open loop}
        \end{figure}
        
        Figure \ref{fig:inverting open loop in} shows the input signal as a waveform.
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.8\textwidth]{opamp/inverting open loop in.png}
          \caption{Open Loop Op Amp Input}
          \label{fig:inverting open loop in}
        \end{figure}

        Figure \ref{fig:inverting open loop out} shows the output signal along with the input.
        The output reaches a maximum of about 8.5V and a minimum of -10V.
        An ideal op amp in open loop configuration would have an infinite gain.
        Here the gain can be calculated roughly as:
        \[\frac{V_{out}}{V_{in}}=\frac{18.5V}{20mv}=\frac{18.5}{0.020}=925\]

        The gain here is most likely capped because of the limits on the power rails.
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.8\textwidth]{opamp/inverting open loop out.png}
          \caption{Open Loop Op Amp Output}
          \label{fig:inverting open loop out}
        \end{figure}

        Using LTspice, I recorded the frequency response of the op amp.
        The bode plot is shown in figure \ref{fig:inverting open loop bode plot}.
        The dotted line is the phase shift frequency response and the solid line is the gain frequency response.
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.8\textwidth]{opamp/inverting open loop bode plot.png}
          \caption{Open Loop Op Amp Frequency Response}
          \label{fig:inverting open loop bode plot}
        \end{figure}

      \subsubsection{Inverting Op Amp 20dB Gain}
        I then set up an op amp with an inverting 20dB gain.
        To do this, I needed to calculate the values of resistors needed.
        I did some brief research using the electronic tutorials website on inverting op amps \cite{Op-AmpInverting}.
        Figure \ref{fig:inverting ex} is taken from here.
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.5\textwidth]{opamp/inverting ex.png}
          \caption{Inverting Op Amp Configuration \cite{Op-AmpInverting}}
          \label{fig:inverting ex}
        \end{figure}

        I knew I needed a gain of 20dB, the relationship between dB and actual gain is:
        \[dB=20\log(\frac{V_{out}}{V_{in}})\]

        Therefore, I calculated I needed a gain of 10:
        \[20=20\log(\frac{V_{out}}{V_{in}})\]
        \[1=\log(\frac{V_{out}}{V_{in}})\]
        \[\frac{V_{out}}{V_{in}}=10^1\]
        
        This is directly related to the resistor values to choose.
        \[\frac{V_{out}}{V_{in}}=-\frac{R{f}}{R_{in}}\]

        Figure \ref{fig:inverting 20} shows how I set up the circuit.
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.8\textwidth]{opamp/inverting 20.png}
          \caption{Inverting Op Amp 20dB Gain Circuit}
          \label{fig:inverting 20}
        \end{figure}

        Figure \ref{fig:inverting 20 out} is the simulation output from this circuit.
        You can see the output signal has been inverted from the input and has a peak to peak voltage of -100mV.
        This is the correct inverting gain of 20dB.
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.8\textwidth]{opamp/inverting 20 out.png}
          \caption{Inverting Op Amp 20dB Gain Circuit Output}
          \label{fig:inverting 20 out}
        \end{figure}

        Using LTspice, I recorded the frequency response of the op amp.
        The bode plot is shown in figure \ref{fig:inverting 20 bode plot}
        The dotted line is the phase shift frequency response and the solid line is the gain frequency response.

        The bandwidth of an operational amplifier is the frequency range where the gain is above -3dB of the maximum value.
        So for this op amp setup, the bandwidth is the frequency range where the gain is above 20dB-3dB, 17dB.
        From this, you can see on the bode plot that the gain falls below 17dB at a frequency of about 95 KHz.
        Therefore, the bandwidth is 95 KHz

        \begin{figure}[H]
          \centering
          \includegraphics[width=0.8\textwidth]{opamp/inverting 20 bode plot.png}
          \caption{Inverting Op Amp 20dB Gain Frequency Response}
          \label{fig:inverting 20 bode plot}
        \end{figure}

    \subsection{Slew Rate Limiting}
        From the lab booklet, slew rate is defined as the rate of change in output voltage caused by a step input in terms of volts per $\mu$s.
        I created a circuit (figure \ref{fig:slew}) to test the slew rate of the LM324 op amp.
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.8\textwidth]{opamp/slew.png}
          \caption{Slew Rate Circuit}
          \label{fig:slew}
        \end{figure}

        \begin{figure}[H]
          \centering
          \includegraphics[width=0.8\textwidth]{opamp/slew out.png}
          \caption{Slew Rate Circuit Output}
          \label{fig:slew out}
        \end{figure}

        From figure \ref{fig:slew out} you can see that the op amp takes about 2.25$\mu$s to reach the input value.
        To calculate the slew rate from this:
        \[Slew rate=\frac{\Delta V}{\Delta t}=\frac{1}{2.25}=0.44\]
        This is close enough to the quoted slew rate of 0.5 on the datasheet \cite{LM324}.

        Rearranging \[Slew rate=2\pi f_p Vpk\] gives \[f_p=\frac{Slew rate}{2\pi Vpk}\]

        Therefore, calculating the Maximum suitable $f_p$ for different peak voltages:
        \begin{table}[H]
          \centering
          \begin{tabular}{|c|c|}
            \hline Vpk & Maximum suitable $f_p$ / KHz\\ 
            \hline 1   & 70.736 \\  
            \hline 5   & 14.147 \\
            \hline 10  & 7.074  \\
            \hline    
          \end{tabular}
          \caption{Slew rate, Voltage and frequency calculations}
          \label{table:slew rate v f}
        \end{table}
    \subsection{Conclusion}
      This task helped me gain more knowledge on some of the AC considerations with op amps.
      It also helped me with the opportunity to lean and use some of the additional tools in LTspice, such as creating the bode plots.
      Completing this lab enabled me to understand in greater detail how the frequency effects the gain of an op amp.

      This was also my first time learning about slew rates which was quite interesting, and I think I gained a good understanding from this lab.
  
  \newpage
  \section{ADC}
    \subsection{Introduction}
      An Analogue to Digital Converter (ADC) converts and analogue signal to a digital signal.
      The objective of this lab is to show the voltage of an analogue input digitally using a bar of LEDs.
    \subsection{Research}
      \subsubsection{Lab background}
        In the lab booklet, we were given a diagram showing the basic function of the counting ADC circuit.
        This is shown in Figure \ref{fig:basic ADC}.
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/basic ADC.png}
          \caption{Basic Counting ADC}
          \label{fig:basic ADC}
        \end{figure}
        
        This also shows the main components that are needed to design the ADC and how they work with each other.
        This is backed up by a parts list that we were also provided with.
        \begin{table}[H]
          \centering
          \begin{tabular}{|c|c|c|c|}
            \hline Part Name                  & Order Number     & Supplier    & Multisim        \\ 
            \hline Bargraph led 74AC00        & 550190           & Rapidonline & BAR\_LED\_BLUE  \\  
            \hline OPA350 rail to rail op amp & 1097443          & Farnell     & OPA350EA        \\
            \hline R2R network                & 8651915          & RS          & 4310R-102-103LF \\
            \hline 74HC573 Latch              & 830034           & Rapidonline & 74hc373n        \\
            \hline 75HC4040 Binary Counter    & 1739782          & RS          & 4040bd\_5V      \\
            \hline 74HC132 Quad NAND          & 1739930          & Farnell     & 74132D or N     \\
            \hline 4116R-1-331 Resistor array & 652-41116R-1-330 & Mouser      & CAT16-3300F8LF  \\
            \hline    
          \end{tabular}
          \caption{Provided parts list}
          \label{table:parts list}
        \end{table}

        However, due to the current pandemic, this lab will have to be completed using simulations without actually physically creating the ADC.
        I will be using Multisim to do this as part numbers for this have been provided in Table \ref{table:parts list}.
      \subsubsection{ADC Research}
    \subsection{Design}
      \subsubsection{Binary Counter}
        My first step in the design of the ADC was to implement the binary counter.
        Using pages 1 and 2 of the datasheet for the MC14040B \cite{MC14040B}, I worked out pin 10 was the clock input, and pin 11 was the reset.

        I used a function generator in Multisim to create the clock signal for the binary counter.

        As I only required an 8-bit output and this is a 12-bit counter, I connected the final four bits to the reset pin.
        This means that when the binary counter reaches 256 (1 0000 0000), the count will reset back to zero (0000 0000).

        My design for this simulation in Multisim can be seen in figure \ref{fig:binary Counter} below.
        I used two 4-input oscilloscopes to view the 8-bit output signal from the binary counter.
        These are shown in different colours to make it easier to understand.
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/binary counter.png}
          \caption{Binary Counter}
          \label{fig:binary Counter}
        \end{figure}

        The first oscilloscope (Figure \ref{fig:binary counter 0 to 3}) shows bits 0-3 of the output.
        The lowest significant bit is the red signal.
        They all have a low value of 0V, and a high value of 5V.
        The traces are positioned separately so they do not overlap so they can be viewed easier.
        Simulating this output clearly shows a divided clock, therefore proving the binary counter works for the first four bits.
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/binary counter 0 to 3.png}
          \caption{Binary Counter Simulation (bits 0-3)}
          \label{fig:binary counter 0 to 3}
        \end{figure}

        The first oscilloscope (Figure \ref{fig:binary counter 4 to 7}) shows bits 4-7 of the output.
        The highest significant bit is the white signal.
        Again, they all have a low value of 0V, and a high value of 5V, and are positioned separately.
        However, the time division scale on the oscilloscope is 10 times larger on this oscilloscope.
        These final four bits also clearly show a divided clock, therefore the entire binary counter is working as expected.
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/binary counter 4 to 7.png}
          \caption{Binary Counter Simulation (bits 4-7)}
          \label{fig:binary counter 4 to 7}
        \end{figure}

      \subsubsection{Latch}
        After successfully getting the binary counter to work, the next step is to implement the latch/buffer circuitry.
        74HC373 is an octal D-type 
        I read the datasheet \cite{74HC373} and found that pin 1 is the output enable pin which is active LOW.
        Pin 11 is the latch enable pin and is active HIGH.
        Table \ref{table:74HC373 Pin description} is an extraction from the datasheet \cite{74HC373}.
        \begin{table}[H]
          \centering
          \begin{tabular}{|c|c|c|}
            \hline Symbol                         & Pin                        & Description                      \\ 
            \hline $\overline{OE}$                & 1                          & output enable input (active LOW) \\  
            \hline Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7 & 2, 5, 6, 9, 12, 15, 16, 19 & 3-state latch output             \\
            \hline D0, D1, D2, D3, D4, D5, D6, D7 & 3, 4, 7, 8, 13, 14, 17, 18 & data input                       \\
            \hline GND                            & 10                         & ground (0 V)                     \\
            \hline LE                             & 11                         & latch enable input (active HIGH) \\
            \hline VCC                            & 20                         & supply voltage                   \\
            \hline    
          \end{tabular}
          \caption{74HC373 Pin description \cite{74HC373}}
          \label{table:74HC373 Pin description}
        \end{table}
        
        Using this information, I added the latch into my Multisim circuit.
        I separately tested changing the output enable input, and changing the latch enable input.
        I could then compare the results to Table \ref{table:74HC373 Functional description} which is also taking from the 74HC373 datasheet \cite{74HC373}.
        \begin{table}[H]
          \centering
          \begin{tabular}{|c|c|c|c|c|c|}
            \hline \multirow{2}{*}{Operating mode} & \multicolumn{2}{|c|}{Control} & Input & \multirow{2}{*}{Internal latches} & Output \\
            \cline{2-4} \cline{6-6} & $\overline{OE}$ & LE & Dn & & Qn \\ 
            \hline \multirow{2}{*}{Enable and read register(transparent mode)} & \multirow{2}{*}{L} & \multirow{2}{*}{H} & L & L & L \\
            \cline{4-6} & & & H & H & H \\
            \hline \multirow{2}{*}{Latch and read register} & \multirow{2}{*}{L} & \multirow{2}{*}{L} & l & L & L \\
            \cline{4-6} & & & h & H & H \\
            \hline \multirow{2}{*}{Latch and read register} & \multirow{2}{*}{H} & \multirow{2}{*}{X} & \multirow{2}{*}{X} & \multirow{2}{*}{X} & \multirow{2}{*}{Z} \\
            & & & & & \\
            \hline
          \end{tabular}
          \caption{74HC373 Functional description \cite{74HC373}}
          \label{table:74HC373 Functional description}
        \end{table}
        
        I used a switch to toggle the input of the latch enable switch from HIGH to LOW.
        Figure \ref{fig:latch latch enable sim} shows the result waveform of the first four bits from this.
        When the input is LOW, the outputs are latched to their current value.
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/latch latch enable.png}
          \caption{Latch Enable Test}
          \label{fig:latch latch enable}
        \end{figure}

        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/latch latch enable sim.png}
          \caption{Latch Enable Test Simulation}
          \label{fig:latch latch enable sim}
        \end{figure}

        
        After testing the latch enable input, I then used another switch to toggle the input of the output enable switch from HIGH to LOW.
        Figure \ref{fig:latch output enable sim} shows the result waveform of the first four bits from this.
        When the input is HIGH, all of the outputs change to an intermediatory value.
        This is acting as $Z$, an high-impedance OFF-state.
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/latch output enable.png}
          \caption{Output Enable Test}
          \label{fig:latch output enable}
        \end{figure}

        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/latch output enable sim.png}
          \caption{Output Enable Test Simulation}
          \label{fig:latch output enable sim}
        \end{figure}

        In my final design, I will implement the output enable pin being changed.
        This will work better with the design as the outputs stop on a HIGH signal.
        Also, there will not be a sufficient output voltage to power the LEDs while this pin is not active.

      \subsubsection{LEDs}
        Adding the LED bar to the circuit was quite simple.
        I replaced the oscilloscope probe inputs with each of the individual LEDs.
        However, it wasn't working on my first attempt but that was because I had the bar the wrong way around, so the polarity was incorrect.

        Figures \ref{fig:led}-\ref{fig:led3} show how the LED bar is connected, and also the first 4 states of the counting procedure on the LEDs.
        After the polarity was correct, they worked as expected.
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/led.png}
          \caption{LEDs 0000 0000 (0)}
          \label{fig:led}
        \end{figure}

        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/led1.png}
          \caption{LEDs 0000 0001 (1)}
          \label{fig:led1}
        \end{figure}

        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/led2.png}
          \caption{LEDs 0000 0010 (2)}
          \label{fig:led2}
        \end{figure}

        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/led3.png}
          \caption{LEDs 0000 0011 (3)}
          \label{fig:led3}
        \end{figure}
      
      \subsubsection{DAC}
        The next step is to create the DAC that will be used with the comparator.
        This will be made using an R2R network.
        I had to use some single pole double throw (SPDT) switches so each bit can be changed from LOW to HIGH.
        At first, my design didn't work because I only used single throw switches.
        This didn't bring the input signals down to ground as they were left not connected when the switch was open.

        When testing my design in Multisim, I used a multimeter to measure the output voltage of the DAC.
        The resolution of the DAC can be calculated as:
        \[Resolution=\frac{V_{max}}{2^{size}}=\frac{5V}{2^{8}}=19.53mV\]
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/dac 00000000.png}
          \caption{DAC 0000 0000 (0)}
          \label{fig:dac 00000000}
        \end{figure}

        Figure \ref{fig:dac 00000000} above shows the DAC with a digital input of 0000 0000, this equates to the decimal value 0.
        This is the minimum value and should be as close to 0V as possible.
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/dac 11111111.png}
          \caption{DAC 1111 1111 (255)}
          \label{fig:dac 11111111}
        \end{figure}

        Figure \ref{fig:dac 11111111} above shows the DAC with a digital input of 1111 1111, this equates to the decimal value 255.
        This is the maximum value and should be as close to 5V as possible.
        The measured value 4.98V is slightly lower than 5V due to the voltage being calculated as:
        \[Output=\frac{input}{2^{size}}.V_{max}=\frac{255}{2^{8}}.5V=\frac{255}{256}.5V=4.98V\]
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/dac 01101010.png}
          \caption{DAC 0110 1010 (106)}
          \label{fig:dac 01101010}
        \end{figure}
        
        Figure \ref{fig:dac 01101010} above shows the DAC with a digital input of 0110 1010, this equates to the decimal value 106.
        The expected output voltage of this can be calculated by:
        \[Output=\frac{input}{2^{size}}.V_{max}=\frac{106}{2^{8}}.5V=\frac{106}{256}.5V=2.07V\]
        This matches the measured value on the multimeter.
      \subsubsection{Comparator}
        A comparator is needed to check if the output from the DAC has reached the requested input voltage.
        I used the OPA350EA \cite{OPA350EA} op amp in open-loop gain as the comparator.
        To assist me with the design, I used figure \ref{fig:opamp} below which is taken from the Electronic Tutorials website \cite{Comparator}.
        \begin{figure}[H]
          \centering
          \includegraphics[width=0.5\textwidth]{adc/opamp.png}
          \caption{Op-amp Comparator Circuit \cite{Comparator}}
          \label{fig:opamp}
        \end{figure}

        I am looking to get a LOW signal from the comparator when the output from the DAC reaches the requested input voltage.
        This would then enable outputs for the latch and gate the clock input to the binary counter.
        As you can see from figure \ref{fig:opamp}, to get a LOW output, $V_{REF}$ would need to be greater than $V_{IN}$.
        This means the output from the DAC needs to be connected to the negative input of the op amp, and then an interactive voltage input to the positive input.
        \begin{figure}[H]
          \centering
          \includegraphics[width=\textwidth]{adc/complete adc.png}
          \caption{Final design with comparator}
          \label{fig:complete adc}
        \end{figure}

        The 74132N NAND Schmitt trigger is used for the gated clock input.
        When the voltage is met, the clock input to the binary counter will be disabled.

    \subsection{Final Testing}
      Figures \ref{fig:complete adc 1}-\ref{fig:complete adc 5} below show the various stages of the final testing of the ADC.
      \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{adc/complete adc 1.png}
        \caption{Final ADC test 1}
        \label{fig:complete adc 1}
      \end{figure}

      In figure \ref{fig:complete adc 1}, the interactive voltage input is set to 1.5V.
      The binary counter is in the process of counting up as shown in the oscilloscope.
      The outputs are disabled on the latch so that the LEDs cannot be seen when counting up.
      \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{adc/complete adc 2.png}
        \caption{Final ADC test 2}
        \label{fig:complete adc 2}
      \end{figure}

      In figure \ref{fig:complete adc 2}, the binary counter has reached the value that is needed for the DAC to output 1.5V.
      The comparator outputs a LOW signal to gate the clock to stop the binary counter and enable the outputs of the latch.
      The allows the LEDs to be powered.
      The output voltage displayed on the LEDs can be calculated by:
      \[Output=\frac{01001101}{2^{8}}.5V=\frac{77}{256}.5V=1.50V\]
      The matches the result required.
      \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{adc/complete adc 3.png}
        \caption{Final ADC test 3}
        \label{fig:complete adc 3}
      \end{figure}

      In figure \ref{fig:complete adc 3}, the interactive voltage input is now set to 4.25V.
      The binary counter is in the process of counting up again as shown in the oscilloscope.
      The outputs are disabled on the latch so that the LEDs cannot be seen when counting up.
      \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{adc/complete adc 4.png}
        \caption{Final ADC test 4}
        \label{fig:complete adc 4}
      \end{figure}

      In figure \ref{fig:complete adc 4}, the binary counter has reached the value that is needed for the DAC to output 4.25V.
      The comparator outputs a LOW signal to gate the clock to stop the binary counter and enable the outputs of the latch.
      The allows the LEDs to be powered.
      The output voltage displayed on the LEDs can be calculated by:
      \[Output=\frac{11011010}{2^{8}}.5V=\frac{218}{256}.5V=4.26V\]
      This is very slightly higher than the result required, this is because of the resolution of the DAC.
      \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{adc/complete adc 5.png}
        \caption{Final ADC test 5}
        \label{fig:complete adc 5}
      \end{figure}

      The final part of the ADC testing is shown in figure \ref{fig:complete adc 5}.
      This is to test moving from a higher input voltage to a lower input voltage.
      As the comparator tests if the DAC output voltage is higher than the interactive input voltage,
      the binary counter will have to be reset so it starts lower than the input voltage.
      This is done by pressing the pushbutton switch S1 which sets the reset pin of the counter temporarily high.

      After this, it will begin to count up until reaching the required value as shown for previous values.
      The interactive voltage input is set to 1.75V.
      The output voltage displayed on the LEDs can be calculated by:
      \[Output=\frac{01011010}{2^{8}}.5V=\frac{90}{256}.5V=1.76V\]
      This is very slightly higher than the result required, this is because of the resolution of the DAC.
    
    \subsection{Conclusion}
      The objective of this task was to design and test an 8-bit Analogue to Digital Converter.
      I managed to do this successfully, however I did not get to test it with physical components and it all had to be done with simulations.
      There is some limitations to this design as it is only using 8-bits.
      Even though the binary counter \cite{MC14040B} is able to use 12-bits, the latch \cite{74HC373} has only 8 inputs.
      Therefore, it made sense to only create an 8-bit DAC using an R2R network.

      A higher resolution ADC would allow for more accurate measurements,
      there have been a few examples during the testing where the reading was off by about 10mV, the higher resolution would have prevented this.
  \newpage
  \listoffigures
  \listoftables
  \bibliographystyle{unsrturl}
  \bibliography{bibliography}
\end{document}